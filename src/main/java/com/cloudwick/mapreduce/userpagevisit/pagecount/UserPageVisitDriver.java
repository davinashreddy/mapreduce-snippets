package com.cloudwick.mapreduce.userpagevisit.pagecount;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


/*
 * Find the no. of times the page has been visited
 * 
 * p1 - 4
 * p2 - 4
 * p3 - 1
 * 
 */

public class UserPageVisitDriver {
	public static void main(String[] args) {

		String input = "input/up_input.in";
		String output = "output/up_output";
		
		try {
			FileUtils.deleteDirectory(new File(output));
			
			Job job = new Job();
			job.setJobName("User Page Visit");
			job.setJarByClass(UserPageVisitDriver.class);
			
			FileInputFormat.addInputPath(job, new Path(input));
			FileOutputFormat.setOutputPath(job, new Path(output));
			
			job.setMapperClass(UserPageVisitMap.class);
			job.setReducerClass(UserPageVisitReducer.class);
			
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(IntWritable.class);
			
			System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
