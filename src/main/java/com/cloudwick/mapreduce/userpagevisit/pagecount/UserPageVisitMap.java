package com.cloudwick.mapreduce.userpagevisit.pagecount;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class UserPageVisitMap extends Mapper<LongWritable, Text, Text, IntWritable> {
	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {
		
		StringTokenizer tokens = new StringTokenizer(value.toString(), ",");
		String page = tokens.nextToken();
		
		context.write(new Text(page), new IntWritable(1));
		
				
	}
}
