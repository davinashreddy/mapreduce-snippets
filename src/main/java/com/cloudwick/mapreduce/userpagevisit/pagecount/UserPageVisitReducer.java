package com.cloudwick.mapreduce.userpagevisit.pagecount;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class UserPageVisitReducer extends
		Reducer<Text, IntWritable, Text, IntWritable> {

	@Override
	protected void reduce(Text key, Iterable<IntWritable> values,
			Reducer<Text, IntWritable, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {
		
		int count = 0;
		
		while(values.iterator().hasNext()) {
			count++;
			values.iterator().next();
		}
		context.write(new Text(key), new IntWritable(count));
	}

}
