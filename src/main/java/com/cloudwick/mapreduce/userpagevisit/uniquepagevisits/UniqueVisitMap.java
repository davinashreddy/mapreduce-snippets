package com.cloudwick.mapreduce.userpagevisit.uniquepagevisits;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class UniqueVisitMap extends Mapper<LongWritable, Text, Text, Text> {
	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, Text, Text>.Context context)
			throws IOException, InterruptedException {

		StringTokenizer tokens = new StringTokenizer(value.toString(), ",");
		String user = tokens.nextToken();
		String page = tokens.nextToken();

		context.write(new Text(page), new Text(user));
	}
}
