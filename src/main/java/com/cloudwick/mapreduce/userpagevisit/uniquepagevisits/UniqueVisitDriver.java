package com.cloudwick.mapreduce.userpagevisit.uniquepagevisits;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


/*
 * Count the unique page visits
 * p1 - 3
 * p2 - 3
 * p3 - 1
 * 
 */
public class UniqueVisitDriver {
	public static void main(String[] args) {
		
		String input = "input/up_input.in";
		String outputDir = "output/up2_output";
		
		try {
			FileUtils.deleteDirectory(new File(outputDir));
		} catch (IOException e) {
			System.out.println("Inside catch clause");
		}
		
		try {
			Job job = new Job();
			job.setJobName("Unique Page Visit");
			job.setJarByClass(UniqueVisitDriver.class);
			
			FileInputFormat.addInputPath(job, new Path(input));
			FileOutputFormat.setOutputPath(job, new Path(outputDir));
			
			job.setMapperClass(UniqueVisitMap.class);
			job.setReducerClass(UniqueVisitReduce.class);
			
			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(Text.class);
			
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(IntWritable.class);
			
			System.exit(job.waitForCompletion(true) ? 1 : 0);
			
		} catch (IllegalStateException illegalStateException) {
			illegalStateException.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
