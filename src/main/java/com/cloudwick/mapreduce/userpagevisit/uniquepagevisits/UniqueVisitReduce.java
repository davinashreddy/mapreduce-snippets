package com.cloudwick.mapreduce.userpagevisit.uniquepagevisits;

import java.io.IOException;
import java.util.HashSet;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class UniqueVisitReduce extends Reducer<Text, Text, Text, IntWritable> {
	@Override
	protected void reduce(Text key, Iterable<Text> values,
			Reducer<Text, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {
		
		HashSet<String> hashSet = new HashSet<String>();
		
		while (values.iterator().hasNext()) {
			hashSet.add(values.iterator().next().toString());
		}
		context.write(new Text(key), new IntWritable(hashSet.size()));
	}
}
