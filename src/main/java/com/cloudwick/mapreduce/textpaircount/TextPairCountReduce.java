package com.cloudwick.mapreduce.textpaircount;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class TextPairCountReduce extends
		Reducer<TextPair, IntWritable, TextPair, IntWritable> {

	@Override
	protected void reduce(TextPair key, Iterable<IntWritable> values,
			Reducer<TextPair, IntWritable, TextPair, IntWritable>.Context context)
			throws IOException, InterruptedException {
		
		System.out.println("Executing reducer");
		System.out.print(key.getFirst() + " " + key.getSecond());
		
		int sum = 0;
		while(values.iterator().hasNext()) {
			sum += values.iterator().next().get();
		}
		
		context.write(key, new IntWritable(sum));
		
	}
}
