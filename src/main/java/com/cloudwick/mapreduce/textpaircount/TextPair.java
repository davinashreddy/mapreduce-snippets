package com.cloudwick.mapreduce.textpaircount;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class TextPair implements WritableComparable<TextPair> {

	private String first;
	private String second;
	
	public TextPair() {
	}

	TextPair(String first, String second) {
		this.first = first;
		this.second = second;
	}
	
	
	public void readFields(DataInput in) throws IOException {
		first = in.readUTF();
		second = in.readUTF();
	}

	public void write(DataOutput out) throws IOException {
		out.writeUTF(first);
		out.writeUTF(second);
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		this.second = second;
	}

	public int compareTo(TextPair o) {
		
		String thisFirst = this.first;
		String thatFirst = o.first;
		
		String thisSecond = this.second;
		String thatSecond = o.second;
		
		if((thisFirst.compareTo(thatFirst) == 0) && (thisSecond.compareTo(thatSecond) == 0)) {
			return 0;
		} else if ((thisFirst.compareTo(thatFirst) != 0) && (thisSecond.compareTo(thatSecond) != 0)) {
			return -1;
		} else return 1;
		
	}
	
	@Override
	public String toString() {
		return first + " " + second;
	}

}
