package com.cloudwick.mapreduce.textpaircount;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class TextPairCountDriver {

	public static void main(String[] args) {
		String input = "input/text_pair.in";
		String output = "output/text_pair_count";
		
		try {
			FileUtils.deleteDirectory(new File(output));
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		try {
			Job job = new Job();
			
			job.setJobName("Text Pair Count using WritableComparable");
			job.setJarByClass(TextPairCountDriver.class);
			
			FileInputFormat.addInputPath(job, new Path(input));
			FileOutputFormat.setOutputPath(job, new Path(output));
			
			job.setMapperClass(TextPairCountMap.class);
			job.setReducerClass(TextPairCountReduce.class);
			
			job.setMapOutputKeyClass(TextPair.class);
			job.setMapOutputValueClass(IntWritable.class);
			
			job.setOutputKeyClass(TextPair.class);
			job.setOutputValueClass(IntWritable.class);
			
			System.exit(job.waitForCompletion(true) ? 1 : 0);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
