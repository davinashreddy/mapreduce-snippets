package com.cloudwick.mapreduce.textpaircount;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class TextPairCountMap extends
		Mapper<LongWritable, Text, TextPair, IntWritable> {
	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, TextPair, IntWritable>.Context context)
			throws IOException, InterruptedException {
		
		System.out.println("Executing map");
		
		String values[] = value.toString().split(" ");
		context.write(new TextPair(values[0], values[1]), new IntWritable(1));
	}
}
