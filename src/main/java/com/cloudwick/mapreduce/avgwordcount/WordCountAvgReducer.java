package com.cloudwick.mapreduce.avgwordcount;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WordCountAvgReducer extends
		Reducer<Text, IntWritable, Text, IntWritable> {

	int c = 0;
	int total = 0;
	int count = 0;

	@Override
	protected void reduce(Text key, Iterable<IntWritable> values,
			Reducer<Text, IntWritable, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {

		System.out.println("Inside Reducer");

		for (IntWritable value : values) {
			count += value.get();

		}
		c++;
		
	}

	@Override
	protected void cleanup(
			Reducer<Text, IntWritable, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {

		System.out.println("Executing cleanup " + c + " " + total + " ,count: " + count);
		context.write(new Text("Avg"), new IntWritable(count / c));

	}
}
