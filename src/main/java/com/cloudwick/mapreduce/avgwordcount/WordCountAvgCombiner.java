package com.cloudwick.mapreduce.avgwordcount;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WordCountAvgCombiner extends
		Reducer<Text, Text, Text, IntWritable> {
	@Override
	protected void reduce(Text key, Iterable<Text> values,
			Reducer<Text, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {

		ArrayList<String> list = new ArrayList<String>();

		while (values.iterator().hasNext()) {
			list.add(values.iterator().next().toString());
		}
		int listSize = list.size();

		while (!list.isEmpty()) {
			String word = list.get(0);
			int count = 0;
			for (String item : list) {
				if (word.equals(item)) {
					count++;
					context.write(new Text(word), new IntWritable(count
							/ listSize));
				}
			}
			list.removeAll(Collections.singleton(word));
			context.write(new Text(word), new IntWritable(count / listSize));
		}
	}
}
