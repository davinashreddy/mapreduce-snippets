package com.cloudwick.mapreduce.avgwordcount;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WordCountAvgMaper extends Mapper<LongWritable, Text, Text, IntWritable> {
	
	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {
		
		StringTokenizer tokens = new StringTokenizer(value.toString());
		
		while (tokens.hasMoreElements()) {
			context.write(new Text(tokens.nextToken()), new IntWritable(1));
		}
	}
}
