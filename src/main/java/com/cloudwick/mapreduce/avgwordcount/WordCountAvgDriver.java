package com.cloudwick.mapreduce.avgwordcount;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class WordCountAvgDriver {

	public static void main(String[] args) {

		String input = "input/avg_wordcount.in";
		String output = "output/wc_output";

		try {
			FileUtils.deleteDirectory(new File(output));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			Job job = new Job();

			job.setJobName("Word Count Average");
			job.setJarByClass(WordCountAvgDriver.class);

			FileInputFormat.addInputPath(job, new Path(input));
			FileOutputFormat.setOutputPath(job, new Path(output));

			job.setMapperClass(WordCountAvgMaper.class);
			job.setReducerClass(WordCountAvgReducer.class);

			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(IntWritable.class);

			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(IntWritable.class);

			System.exit(job.waitForCompletion(true) ? 1 : 0);
	
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
