package com.cloudwick.mapreduce.clickstream;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class ClickMap extends Mapper<LongWritable, Text, Text, Text> {
	
	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, Text, Text>.Context context)
			throws IOException, InterruptedException {
		

		String[] items = value.toString().split(" ");

		String ipaddr = items[0];
		String date = items[3];
		String site = items[10];
		
		if(date.contains("2002")) {
			context.write(new Text(site), new Text(ipaddr));
		}
	}
}
