package com.cloudwick.mapreduce.clickstream;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class ClickStreamDriver {
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
		
		if (args.length != 2) {
			System.err.println("Please pass \"input path\" &  \"output path\" as arguments");
			System.exit(-1);
		}
		
		Job job = new Job();
		job.setJarByClass(ClickStreamDriver.class);
		
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
		job.setMapperClass(ClickMap.class);
		job.setReducerClass(ClickReduce.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
