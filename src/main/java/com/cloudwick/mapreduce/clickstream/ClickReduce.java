package com.cloudwick.mapreduce.clickstream;

import java.io.IOException;
import java.util.HashSet;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ClickReduce extends Reducer<Text, Text, Text, IntWritable> {
	@Override
	protected void reduce(Text key, Iterable<Text> values,
			Reducer<Text, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {

		HashSet<String> hs = new HashSet<String>();

		while (values.iterator().hasNext()) {
			hs.add(values.iterator().next().toString());
		}
		context.write(new Text(key), new IntWritable(hs.size()));

	}
}
