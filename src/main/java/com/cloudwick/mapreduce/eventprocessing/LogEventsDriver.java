package com.cloudwick.mapreduce.eventprocessing;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class LogEventsDriver {

	public static void main(String[] args) {

		String input = "input/mock_apache_pool-1-thread-1.data";
		String output = "output/log_events";

		try {
			FileUtils.deleteDirectory(new File(output));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			Job job = new Job();
			
			job.setJarByClass(LogEventsDriver.class);
			job.setJobName("Log Event Processing");
			
			FileInputFormat.addInputPath(job, new Path(input));
			FileOutputFormat.setOutputPath(job, new Path(output));
			
			job.setMapperClass(LogEventsMapper.class);
			job.setReducerClass(LogEventsReducer.class);
			
			job.setMapOutputKeyClass(LogEventWriter.class);
			job.setMapOutputValueClass(IntWritable.class);
			
			job.setOutputKeyClass(LogEventsDriver.class);
			job.setOutputValueClass(IntWritable.class);
			
			
			
			System.exit(job.waitForCompletion(true) ? 1 : 0);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

}
