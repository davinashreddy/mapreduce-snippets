package com.cloudwick.mapreduce.eventprocessing;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class LogEventsReducer extends
		Reducer<LogEventWriter, IntWritable, LogEventWriter, IntWritable> {
	@Override
	protected void reduce(
			LogEventWriter key,
			Iterable<IntWritable> values,
			Reducer<LogEventWriter, IntWritable, LogEventWriter, IntWritable>.Context context)
			throws IOException, InterruptedException {
		
		
		int count = 0;
		
		while(values.iterator().hasNext()) {
			count += values.iterator().next().get();
		}
		
		context.write(key, new IntWritable(count));
	}
}
