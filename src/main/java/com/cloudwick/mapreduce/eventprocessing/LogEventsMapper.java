package com.cloudwick.mapreduce.eventprocessing;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class LogEventsMapper extends Mapper<LongWritable, Text, LogEventWriter, IntWritable> {

	@Override
	protected void map(
			LongWritable key,
			Text value,
			Mapper<LongWritable, Text, LogEventWriter, IntWritable>.Context context)
			throws IOException, InterruptedException {
		
		/*
		 * 0: 182.84.15.120 1: - 2: - 3: [2014-07-19T16:05:42Z] 4: "GET 5:
		 * /services.html 6: HTTP/1.1" 7: 503 8: 762 9: "-" 10: "Mozilla/5.0 11: (X11;
		 * 12: Linux 13: x86_64; 14: rv:6.0a1) 15: Gecko/20110421 16: Firefox/6.0a1"
		 */
		
		String[] fields = value.toString().split(" ");
		
		String ipaddress = fields[0];
		String request_page = fields[5];
		String timestamp = fields[3];
		
		context.write(new LogEventWriter(ipaddress, request_page, timestamp), new IntWritable(1));
	}
}
