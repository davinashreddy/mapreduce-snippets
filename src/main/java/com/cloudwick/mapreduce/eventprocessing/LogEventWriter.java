package com.cloudwick.mapreduce.eventprocessing;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class LogEventWriter implements WritableComparable<LogEventWriter> {

	String ipaddress;
	String request_page;
	String timestamp;
	
	public LogEventWriter() {
	}
	
	public LogEventWriter(String ipaddress, String request_page, String timestamp) {
		this.ipaddress = ipaddress;
		this.request_page = request_page;
		this.timestamp = timestamp;
	}
	
	public void readFields(DataInput in) throws IOException {
		ipaddress = in.readUTF();
		request_page = in.readUTF();
		timestamp = in.readUTF();
	}

	public void write(DataOutput out) throws IOException {
		out.writeUTF(ipaddress);
		out.writeUTF(request_page);
		out.writeUTF(timestamp);
	}

	public int compareTo(LogEventWriter obj) {
		if(ipaddress.compareTo(obj.ipaddress) == 0) {
			return 0;
		} else if (ipaddress.compareTo(obj.ipaddress) > 0) {
			return 1;
		} else {
			return -1;
		}
	}

	@Override
	public String toString() {
		return "(" + ipaddress + ", " + timestamp + ", " + request_page + ")  ";
	}
}
