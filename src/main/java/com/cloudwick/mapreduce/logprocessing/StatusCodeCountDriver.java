package com.cloudwick.mapreduce.logprocessing;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


public class StatusCodeCountDriver {

	public static void main(String[] args) {
		
		String input = "input/mock_apache_pool-1-thread-1.data";
		String output = "output/status_code_count";
		
		try {
			FileUtils.deleteDirectory(new File(output));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			Job job = new Job();
			
			FileInputFormat.addInputPath(job, new Path(input));
			FileOutputFormat.setOutputPath(job, new Path(output));
			
			job.setJobName("Status code Count");
			job.setJarByClass(StatusCodeCountDriver.class);
			
			job.setMapperClass(StatusCodeCountMapper.class);
			job.setReducerClass(StatusCodeCountReducer.class);
			
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(IntWritable.class);
			
			
			System.exit(job.waitForCompletion(true) ? 1 : 0);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
