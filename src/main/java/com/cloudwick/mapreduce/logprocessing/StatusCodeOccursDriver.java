package com.cloudwick.mapreduce.logprocessing;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;

import com.sun.jersey.core.impl.provider.entity.XMLJAXBElementProvider.Text;

public class StatusCodeOccursDriver {

	public static void main(String[] args) {
		
		String input = "input/mock_apache_pool-1-thread-1.data";
		String output = "output/status_code_occurs";
		
		try {
			FileUtils.deleteDirectory(new File(output));
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		try {
			Job job = new Job();
			
			FileInputFormat.setInputPaths(job, new Path(input));
			FileOutputFormat.setOutputPath(job, new Path(output));
			
			job.setNumReduceTasks(0);
			
			job.setMapperClass(StatusCodeOccursMap.class);
			
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(NullOutputFormat.class);
			
			job.waitForCompletion(false);
			
			//System.exit(job.waitForCompletion(true) ? 1 : 0);
			Counters counters = job.getCounters();
			System.out.println("times: " + counters.findCounter(Counter.COUNTER).getValue());
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
