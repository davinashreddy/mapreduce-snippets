package com.cloudwick.mapreduce.logprocessing;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class StatusCodeOccursMap extends
		Mapper<LongWritable, Text, Text, NullWritable> {

	@Override
	protected void map(
			LongWritable key,
			Text value,
			Mapper<LongWritable, Text, Text, NullWritable>.Context context)
			throws IOException, InterruptedException {
		
		String[] fields = value.toString().split(" ");

		if(fields[7].equals("404") || fields[7].equals("200") || fields[7].equals("503")) {
			context.getCounter(Counter.COUNTER).increment(1L);
		}
		
		

		//context.write(value, NullWritable.get());
		
		
	}

}
