package com.cloudwick.mapreduce.logprocessing;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class StatusCodeCountMapper extends
		Mapper<LongWritable, Text, Text, IntWritable> {

	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {

		String[] fields = value.toString().split(" ");

		if(fields[7].equals("404") || fields[7].equals("200") || fields[7].equals("503")) {
			context.write(new Text(fields[7]), new IntWritable(1));
		}
		
	}
}

// 25.198.250.35 - - [2014-07-19T16:05:33Z] "GET / HTTP/1.1" 404 1081
// "-"
// "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)"
/*
 * 0: 182.84.15.120 1: - 2: - 3: [2014-07-19T16:05:42Z] 4: "GET 5:
 * /services.html 6: HTTP/1.1" 7: 503 8: 762 9: "-" 10: "Mozilla/5.0 11: (X11;
 * 12: Linux 13: x86_64; 14: rv:6.0a1) 15: Gecko/20110421 16: Firefox/6.0a1"
 */